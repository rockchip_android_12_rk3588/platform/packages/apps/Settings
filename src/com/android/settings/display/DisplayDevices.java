package com.android.settings.display;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.IBinder;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.IWindowManager;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.os.IRadxaOverlay;
import android.os.RemoteException;
import androidx.fragment.app.DialogFragment;
import androidx.preference.CheckBoxPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.SwitchPreference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceScreen;
import com.android.internal.logging.nano.MetricsProto.MetricsEvent;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import android.widget.Toast;

import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.Process;
import java.lang.Runtime;
import java.lang.InterruptedException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class DisplayDevices extends SettingsPreferenceFragment
    implements Preference.OnPreferenceChangeListener,
               Preference.OnPreferenceClickListener {

    private static final String TAG = "displaydevices";
    private static final String KEY_HDMI0 = "hdmi0_display";
    private static final String KEY_HDMI1 = "hdmi1_display";
    private static final String KEY_EDP0 = "edp0_display";
    private static final String KEY_EDP1 = "edp1_display";
    private static final String KEY_DP0 = "dp0_display";
    private static final String KEY_DP1 = "dp1_display";
    private static final String KEY_DSI0 = "dsi0_display";
    private static final String KEY_DSI0_PANEL = "dsi0_panel";
    private static final String KEY_DSI1 = "dsi1_display";
    private static final String KEY_DSI1_PANEL = "dsi1_panel";
    private static final String KEY_DISPLAY = "display_summary";
    private static final String default_option = "-1";


    private Context mContext;
    private SharedPreferences DisplayPrefs;
    //vop<--->connect map
    private Map<Integer, Integer> vopMap;
    private int mRotation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED;
    private ListPreference mHdmi0;
    private ListPreference mHdmi1;
    private ListPreference mEdp0;
    private ListPreference mEdp1;
    private ListPreference mDp0;
    private ListPreference mDp1;
    private ListPreference mDsi0;
    private ListPreference mDsi0_panel;
    private ListPreference mDsi1;
    private ListPreference mDsi1_panel;
    private String option = "-1";
    private String ProductDevice = "rockchip";   //ro.product.device
    private IRadxaOverlay radxaoverlay_interface;

    //display item id
    private static final int VENDOR_RADXA_DISPLAY_ID = 32;

    private static final int RADXA_CONNECTOR_HDMI0 = 0;
    private static final int RADXA_CONNECTOR_HDMI1 = 1;
    private static final int RADXA_CONNECTOR_DP0 = 2;
    private static final int RADXA_CONNECTOR_DP1 = 3;
    private static final int RADXA_CONNECTOR_EDP0 = 4;
    private static final int RADXA_CONNECTOR_EDP1 = 5;
    private static final int RADXA_CONNECTOR_DSI0 = 6;
    private static final int RADXA_CONNECTOR_DSI1 = 7;
    /*
       Display string format
        x-xxxx-xxxx-xxxx... --> 8-0000-1000-2000-3000-4000-5000-6000-7000
        x
            第一个X表示有几种CONNECTOR
        xxxx
            屏类型---hdmi0-1/dp0-1/dsi0-1
            子类型---dsi(8hd,10hd....)
            vop---(0,1,2,3)
            使能标志---(0,1)
   */
    private String display_devices = "8-0000-1000-2000-3000-4000-5000-6000-7000";
    private String display_data = "";
    private int hdmi0_startIndex = 3;
    private int hdmi1_startIndex = 8;
    private int dp0_startIndex = 13;
    private int dp1_startIndex = 18;
    private int edp0_startIndex = 23;
    private int edp1_startIndex = 28;
    private int dsi0_startIndex = 33;
    private int dsi1_startIndex = 38;
    private int replace_len = 2;

    private String hdmi0_init = "-1";
    private String hdmi1_init = "-1";
    private String dp0_int = "-1";
    private String dp1_int = "-1";
    private String edp0_init = "-1";
    private String edp1_init = "-1";
    private String dsi0_init = "-1";
    private String dsi0_panel_init = "0";
    private String dsi1_init = "-1";
    private String dsi1_panel_init = "0";

    @Override
    public int getMetricsCategory() {
        return MetricsEvent.DISPLAY;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "onCreate----------------------------------------");
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mRotation = getActivity().getRequestedOrientation();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        IBinder service = ServiceManager.getService(Context.RADXAOVERLAY_SERVICE);
        if (service != null) {
            radxaoverlay_interface  = IRadxaOverlay.Stub.asInterface(service);
        }

        String val = android.os.SystemProperties.get("ro.product.device");
        if (val.equals("Radxa_PS009")) {
            addPreferencesFromResource(R.xml.Radxa_PS009_display_devices);
        }else if (val.equals("RadxaRock5C")) {
            addPreferencesFromResource(R.xml.Radxa_Rock5C_display_devices);
        }else if (val.equals("RadxaRock5B")) {
            addPreferencesFromResource(R.xml.Radxa_Rock5B_display_devices);
        }else if (val.equals("RadxaCM5")) {
            addPreferencesFromResource(R.xml.Radxa_CM5_display_devices);
        }else{
            addPreferencesFromResource(R.xml.display_devices);
        }

        DisplayPrefs = mContext.getSharedPreferences("display_prefs", Context.MODE_PRIVATE);

        val = android.os.SystemProperties.get("vendor.radxa.display.init");
        if(DisplayPrefs.contains(KEY_DISPLAY)) {
            display_data = DisplayPrefs.getString(KEY_DISPLAY,val);
            Log.d(TAG, "onCreate-------read key------display_data:"+ display_data);
            val = display_data;
        } else {
            DisplayPrefs.edit()
                .putString(KEY_DISPLAY,val)
                .commit();
            display_data = val;
            Log.d(TAG, "onCreate-------creat key------display_data:"+ val);
        }

        //使能标志位
        String enable = val.substring(hdmi0_startIndex+2,hdmi0_startIndex+3);
        if(enable.equals("1"))
            hdmi0_init = val.substring(hdmi0_startIndex+1,hdmi0_startIndex+2);

        enable = val.substring(hdmi1_startIndex+2,hdmi1_startIndex+3);
        if(enable.equals("1"))
            hdmi1_init = val.substring(hdmi1_startIndex+1,hdmi1_startIndex+2);

        enable = val.substring(dp0_startIndex+2,dp0_startIndex+3);
        if(enable.equals("1"))
            dp0_int = val.substring(dp0_startIndex+1,dp0_startIndex+2);

        enable = val.substring(dp1_startIndex+2,dp1_startIndex+3);
        if(enable.equals("1"))
            dp1_int = val.substring(dp1_startIndex+1,dp1_startIndex+2);

        enable = val.substring(edp0_startIndex+2,edp0_startIndex+3);
        if(enable.equals("1"))
            edp0_init = val.substring(edp0_startIndex+1,edp0_startIndex+2);

        enable = val.substring(edp1_startIndex+2,edp1_startIndex+3);
        if(enable.equals("1"))
            edp1_init = val.substring(edp1_startIndex+1,edp1_startIndex+2);

        enable = val.substring(dsi0_startIndex+2,dsi0_startIndex+3);
        if(enable.equals("1")){
            dsi0_init = val.substring(dsi0_startIndex+1,dsi0_startIndex+2);
            dsi0_panel_init = val.substring(dsi0_startIndex,dsi0_startIndex+1);
        }

        enable = val.substring(dsi1_startIndex+2,dsi1_startIndex+3);
        if(enable.equals("1")){
            dsi1_init = val.substring(dsi1_startIndex+1,dsi1_startIndex+2);
            dsi1_panel_init = val.substring(dsi1_startIndex,dsi1_startIndex+1);
        }

        vopMap = new HashMap<>();
        String[] tokens = display_data.split("-");//tokens分割的字串数组
        for (int i = 1; i <= 8; i++) { //跳过display_data最前的"8-"
            String subString = tokens[i].substring(tokens[i].length() - 1);//最后一个字符,表使能
            int value = Integer.parseInt(tokens[i].substring(0, 1)); //第一个字符,表connect器
            int key = Integer.parseInt(tokens[i].substring(tokens[i].length() - 2,tokens[i].length() - 1)); //倒数第二个字符,表vop
            if (subString.equals("1")) { //最后一个字符为1,代表对应的connect使能
                vopMap.put(key, value);//写入
            }
        }
        Log.d(TAG, "onCreate----------------------------------vop-conncet map size:"+vopMap.size());

        mHdmi0 = (ListPreference) findPreference(KEY_HDMI0);
        if (mHdmi0 != null) {
            mHdmi0.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_HDMI0,hdmi0_init);
            mHdmi0.setValue(option);
        }else{
            Log.i(TAG, "ListPreference hdmi0 NULL");
        }

        mHdmi1 = (ListPreference) findPreference(KEY_HDMI1);
        if (mHdmi1 != null) {
            mHdmi1.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_HDMI1,hdmi1_init);
            mHdmi1.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Hdmi1 NULL");
        }

        mEdp0 = (ListPreference) findPreference(KEY_EDP0);
        if (mEdp0 != null) {
            mEdp0.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_EDP0,edp0_init);
            mEdp0.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Edp0 NULL");
        }

        mEdp1 = (ListPreference) findPreference(KEY_EDP1);
        if (mEdp1 != null) {
            mEdp1.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_EDP1,edp1_init);
            mEdp1.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Edp1 NULL");
        }

        mDp0 = (ListPreference) findPreference(KEY_DP0);
        if (mDp0 != null) {
            mDp0.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_DP0,dp0_int);
            mDp0.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Dp0 NULL");
        }

        mDp1 = (ListPreference) findPreference(KEY_DP1);
        if (mDp1 != null) {
            mDp1.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_DP1,dp1_int);
            mDp1.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Dp1 NULL");
        }

        mDsi0 = (ListPreference) findPreference(KEY_DSI0);
        if (mDsi0 != null) {
            mDsi0.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_DSI0,dsi0_init);
            mDsi0.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Dsi0 NULL");
        }

        mDsi0_panel = (ListPreference) findPreference(KEY_DSI0_PANEL);
        if (mDsi0_panel != null) {
            mDsi0_panel.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_DSI0_PANEL,dsi0_panel_init);
            mDsi0_panel.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Dsi0_panel NULL");
        }

        mDsi1 = (ListPreference) findPreference(KEY_DSI1);
        if (mDsi1 != null) {
            mDsi1.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_DSI1,dsi1_init);
            mDsi1.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Dsi1 NULL");
        }

        mDsi1_panel = (ListPreference) findPreference(KEY_DSI1_PANEL);
        if (mDsi1_panel != null) {
            mDsi1_panel.setOnPreferenceChangeListener(this);
            option = DisplayPrefs.getString(KEY_DSI1_PANEL,dsi1_panel_init);
            mDsi1_panel.setValue(option);
        }else{
            Log.i(TAG, "ListPreference Dsi1_panel NULL");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView----------------------------------------");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public static boolean isAvailable() {
        return true;
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        Log.i(TAG, "onPreferenceClick " + key);
        if (key.startsWith(KEY_HDMI0)) {

        } 
        return true;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object obj) {
        String key = preference.getKey();
        int connector = -1;
        String valueToStore = String.valueOf(obj);

        Log.i(TAG, key + " onPreferenceChange:" + obj);

        //1. vop与connector之间的设置
        if( !(key.startsWith(KEY_DSI1_PANEL) ||  key.startsWith(KEY_DSI0_PANEL)) ){
            Integer connectorType = vopMap.get(Integer.parseInt(valueToStore));//vop(0~3)-->connector(0~7)
            //当前VOP是否已设置对应的connectorType
            if (connectorType != null) {
                Log.i(TAG, "onPreferenceChange: connector already use:" + connectorType);
                //获取现在想设置的 connectorType
                connector = getConnectorType(key);
                Log.i(TAG, "onPreferenceChange: set new connect :" + connector);

                //同一个connectorType上的设置
                if (connector == connectorType.intValue()) {
                    if(!valueToStore.equals("-1")){//非none选项,需要先移除早先map,再加入新的map

                        //移除早先的map选项
                        Integer targetKey;
                        for (Map.Entry<Integer, Integer> entry : vopMap.entrySet()) {
                            if (entry.getValue().equals(connectorType)) {
                                targetKey = entry.getKey();
                                Log.i(TAG, "onPreferenceChange: remove vop option  :" + targetKey);
                                vopMap.remove(targetKey);
                                break;
                            }
                        }

                        //加入现在的map
                        vopMap.put(Integer.parseInt(valueToStore), connector);
                    }else{ //none 选项.需要从vopmap中移除
                        vopMap.remove(Integer.parseInt(valueToStore));
                    }
                } else {
                    //提醒用户该VOP已被其它connectorType占用
                    Toastinfo(Integer.parseInt(valueToStore),connectorType);
                    //刷新当前UI,设定为先前的值
                    preferencerefresh(key);
                    return false;
                }
            } else { //还未设置的VOP的项 或 将某个VOP设置为NONE
                //初次设置该VOP时,其对应的connectorType可能已被占用
                /*
                   vop	    connector
                   0   	0  ---第一次设置
                   1   	0  ---第二次设置
                   */
                //获取现在想设置的 connectorType
                connector = getConnectorType(key);
                //移除早先含当前要设置 connectorType 的map选项
                Integer targetKey;
                for (Map.Entry<Integer, Integer> entry : vopMap.entrySet()) {
                    if (entry.getValue().equals(connector)) {
                        targetKey = entry.getKey();
                        Log.i(TAG, "onPreferenceChange: remove vop option  :" + targetKey);
                        vopMap.remove(targetKey);
                        break;
                    }
                }
                //加入现在的map
                if(!valueToStore.equals("-1")){
                    connector = getConnectorType(key);
                    vopMap.put(Integer.parseInt(valueToStore), connector);
                }
            }

            for (Map.Entry<Integer, Integer> entry : vopMap.entrySet()) {
                Log.d(TAG,"Key: " + entry.getKey() + ", Value: " + entry.getValue());
            }
        }


        //2.preference 事件保存到 SharedPreferences
        if (key.startsWith(KEY_HDMI0) && preference ==  mHdmi0) {
            DisplayPrefs.edit()
                .putString(KEY_HDMI0,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,hdmi0_startIndex,valueToStore);
        } else if (key.startsWith(KEY_HDMI1) && preference ==  mHdmi1) {
            DisplayPrefs.edit()
                .putString(KEY_HDMI1,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,hdmi1_startIndex,valueToStore);
        } else if (key.startsWith(KEY_EDP0) && preference ==  mEdp0) {
            DisplayPrefs.edit()
                .putString(KEY_EDP0,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,edp0_startIndex,valueToStore);
        } else if (key.startsWith(KEY_EDP1) && preference ==  mEdp1) {
            DisplayPrefs.edit()
                .putString(KEY_EDP1,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,edp1_startIndex,valueToStore);
        } else if (key.startsWith(KEY_DP0) && preference ==  mDp0) {
            DisplayPrefs.edit()
                .putString(KEY_DP0,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,dp0_startIndex,valueToStore);
        } else if (key.startsWith(KEY_DP1) && preference ==  mDp1) {
            DisplayPrefs.edit()
                .putString(KEY_DP1,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,dp1_startIndex,valueToStore);
        } else if (key.startsWith(KEY_DSI0) && preference ==  mDsi0) {
            DisplayPrefs.edit()
                .putString(KEY_DSI0,valueToStore)
                .commit();
            display_data = OverlayDataTrans(display_data,dsi0_startIndex,valueToStore);
        } else if (key.startsWith(KEY_DSI0_PANEL) && preference ==  mDsi0_panel) {
            DisplayPrefs.edit()
                .putString(KEY_DSI0_PANEL,valueToStore)
                .commit();
            display_data = OverlayDataTransPanel(display_data,dsi0_startIndex,valueToStore);
        } else if (key.startsWith(KEY_DSI1) && preference ==  mDsi1) {
            DisplayPrefs.edit()
                .putString(KEY_DSI1,valueToStore)
                .commit();;
            display_data = OverlayDataTrans(display_data,dsi1_startIndex,valueToStore);
        }if (key.startsWith(KEY_DSI1_PANEL) && preference ==  mDsi1_panel) {
            DisplayPrefs.edit()
                .putString(KEY_DSI1_PANEL,valueToStore)
                .commit();
            display_data = OverlayDataTransPanel(display_data,dsi1_startIndex,valueToStore);
        }

        DisplayPrefs.edit()
            .putString(KEY_DISPLAY,display_data)
            .commit();

        try {
            radxaoverlay_interface.overlay_write(VENDOR_RADXA_DISPLAY_ID,display_data);
        }catch (RemoteException e) {
            e.printStackTrace();
        }

        return true;
    }

    private void preferencerefresh(String key) {
        if (key.startsWith(KEY_HDMI0)){
            option = DisplayPrefs.getString(KEY_HDMI0,"-1");
            mHdmi0.setValue(option);
        } else if (key.startsWith(KEY_HDMI1)){
            option = DisplayPrefs.getString(KEY_HDMI1,"-1");
            mHdmi1.setValue(option);
        } else if (key.startsWith(KEY_DP0)){
            option = DisplayPrefs.getString(KEY_DP0,"-1");
            mDp0.setValue(option);
        } else if (key.startsWith(KEY_DP1)){
            option = DisplayPrefs.getString(KEY_DP1,"-1");
            mDp1.setValue(option);
        } else if (key.startsWith(KEY_EDP0)){
            option = DisplayPrefs.getString(KEY_EDP0,"-1");
            mEdp0.setValue(option);
        } else if (key.startsWith(KEY_EDP1)){
            option = DisplayPrefs.getString(KEY_EDP1,"-1");
            mEdp1.setValue(option);
        } else if (key.startsWith(KEY_DSI0)){
            option = DisplayPrefs.getString(KEY_DSI0,"-1");
            mDsi0.setValue(option);
        } else if (key.startsWith(KEY_DSI1)){
            option = DisplayPrefs.getString(KEY_DSI1,"-1");
            mDsi1.setValue(option);
        }
    }


    private int getConnectorType(String key) {
        int connector = -1;
        if (key.startsWith(KEY_HDMI0)){
            connector = 0;
        } else if (key.startsWith(KEY_HDMI1)){
            connector = 1;
        } else if (key.startsWith(KEY_DP0)){
            connector = 2;
        } else if (key.startsWith(KEY_DP1)){
            connector = 3;
        } else if (key.startsWith(KEY_EDP0)){
            connector = 4;
        } else if (key.startsWith(KEY_EDP1)){
            connector = 5;
        } else if (key.startsWith(KEY_DSI0)){
            connector = 6;
        } else if (key.startsWith(KEY_DSI1)){
            connector = 7;
        }

        return connector;
    }

    private void Toastinfo(int vop,int connector) {
        String connector_info = "";
        Log.i(TAG," Toastinfo:" + connector);
        switch (connector){
            case RADXA_CONNECTOR_HDMI0:
                connector_info = "HDMI0";
                break;
            case RADXA_CONNECTOR_HDMI1:
                connector_info = "HDMI1";
                break;
            case RADXA_CONNECTOR_DP0:
                connector_info = "DP0";
                break;
            case RADXA_CONNECTOR_DP1:
                connector_info = "DP1";
                break;
            case RADXA_CONNECTOR_EDP0:
                connector_info = "EDP0";
                break;
            case RADXA_CONNECTOR_EDP1:
                connector_info = "EDP1";
                break;
            case RADXA_CONNECTOR_DSI0:
                connector_info = "DSI0";
                break;
            case RADXA_CONNECTOR_DSI1:
                connector_info = "DSI1";
                break;
            default:
                connector_info = "none";
                break;
        }
        String info = "The current vop is already used by " + connector_info  + " , please close the VOP of " + connector_info;
        Toast.makeText(mContext, info, Toast.LENGTH_SHORT).show();
    }


    private String OverlayDataTransPanel(String display_data,int startIndex,String valueToStore) {
        String replacement = "0";

        if(valueToStore.equals("0"))
            replacement = "0";
        else if(valueToStore.equals("1"))
            replacement = "1";
        else if(valueToStore.equals("2"))
            replacement = "2";
        else if(valueToStore.equals("3"))
            replacement = "3";
        else if(valueToStore.equals("4"))
            replacement = "4";
        else if(valueToStore.equals("5"))
            replacement = "5";

        String prefix = display_data.substring(0, startIndex);
        String suffix = display_data.substring(startIndex + 1);
        display_data = prefix + replacement + suffix;
        return  display_data;
    }

    private String OverlayDataTrans(String display_data,int startIndex,String valueToStore) {
        String replacement = "00";

        startIndex++;

        if(valueToStore.equals("-1"))
            replacement = "00";
        else if(valueToStore.equals("0"))
            replacement = "01";
        else if(valueToStore.equals("1"))
            replacement = "11";
        else if(valueToStore.equals("2"))
            replacement = "21";
        else if(valueToStore.equals("3"))
            replacement = "31";

        String prefix = display_data.substring(0, startIndex);
        String suffix = display_data.substring(startIndex + replace_len);
        display_data = prefix + replacement + suffix;
        return  display_data;
    }

}
